class CreateHouses < ActiveRecord::Migration
  def change
    create_table :houses do |t|
      t.string :size
      t.string :owner

      t.timestamps null: false
    end
  end
end
