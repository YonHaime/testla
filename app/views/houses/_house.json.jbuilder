json.extract! house, :id, :size, :owner, :created_at, :updated_at
json.url house_url(house, format: :json)
